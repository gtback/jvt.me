---
title: 2023's Music In Review
description: What music was I listening to in 2023?
date: 2024-01-04T14:50:07+0000
---

In 2023, I listened to 65531.62 minutes (1092.19 hours) of music on Spotify.

<details>
  <summary>Top 100 songs</summary>
  <table>
    <tr>
      <th>Song Title</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Phaeleh - Moving On</td>
      <td>325.42</td>
      <td>5.42</td>
    </tr>
    <tr>
      <td>REAPER - XLR8 - Audioscribe Remix</td>
      <td>224.37</td>
      <td>3.74</td>
    </tr>
    <tr>
      <td>Phaeleh - Dreaming</td>
      <td>212.35</td>
      <td>3.54</td>
    </tr>
    <tr>
      <td>Phaeleh - Breathe in Air</td>
      <td>198.9</td>
      <td>3.32</td>
    </tr>
    <tr>
      <td>Phaeleh - The Mist</td>
      <td>197.2</td>
      <td>3.29</td>
    </tr>
    <tr>
      <td>Paper Dragon - Self Control - Coastal Remix</td>
      <td>191.45</td>
      <td>3.19</td>
    </tr>
    <tr>
      <td>Koven - YES - Fox Stevenson Remix</td>
      <td>183.3</td>
      <td>3.06</td>
    </tr>
    <tr>
      <td>deadmau5 - Bridged By A Lightwave - Lamorn Remix</td>
      <td>167.78</td>
      <td>2.8</td>
    </tr>
    <tr>
      <td>Rag'n'Bone Man - All You Ever Wanted - Sub Focus Remix</td>
      <td>136.98</td>
      <td>2.28</td>
    </tr>
    <tr>
      <td>Phaeleh - One Chance</td>
      <td>121.08</td>
      <td>2.02</td>
    </tr>
    <tr>
      <td>TARO - I Saw Fire</td>
      <td>111.13</td>
      <td>1.85</td>
    </tr>
    <tr>
      <td>cassö - Prada</td>
      <td>108.17</td>
      <td>1.8</td>
    </tr>
    <tr>
      <td>Lexurus - The Distance</td>
      <td>105.68</td>
      <td>1.76</td>
    </tr>
    <tr>
      <td>Feint - Let Me Go</td>
      <td>105.37</td>
      <td>1.76</td>
    </tr>
    <tr>
      <td>Paperwhite - Only Us (Remix)</td>
      <td>102.97</td>
      <td>1.72</td>
    </tr>
    <tr>
      <td>Lexurus - Magnify - Justin Hawkes Remix</td>
      <td>101.52</td>
      <td>1.69</td>
    </tr>
    <tr>
      <td>Puppet - The Worst Part About Me</td>
      <td>97.87</td>
      <td>1.63</td>
    </tr>
    <tr>
      <td>MRSA - Different</td>
      <td>97.1</td>
      <td>1.62</td>
    </tr>
    <tr>
      <td>Exploid - These Lies</td>
      <td>95.85</td>
      <td>1.6</td>
    </tr>
    <tr>
      <td>EMBRZ - IOU</td>
      <td>93.32</td>
      <td>1.56</td>
    </tr>
    <tr>
      <td>Subformat - More</td>
      <td>90.05</td>
      <td>1.5</td>
    </tr>
    <tr>
      <td>OX7GEN - Emergence</td>
      <td>86.8</td>
      <td>1.45</td>
    </tr>
    <tr>
      <td>Tycho - Epoch</td>
      <td>85.42</td>
      <td>1.42</td>
    </tr>
    <tr>
      <td>REPAIR - Pretend</td>
      <td>85.4</td>
      <td>1.42</td>
    </tr>
    <tr>
      <td>Florian Picasso - Final Call - VIP Mix</td>
      <td>82.82</td>
      <td>1.38</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Slay</td>
      <td>80.48</td>
      <td>1.34</td>
    </tr>
    <tr>
      <td>Clean Bandit - Don't Leave Me Lonely (feat. Elley Duhé) [Punctual Remix]</td>
      <td>75.0</td>
      <td>1.25</td>
    </tr>
    <tr>
      <td>Phaeleh - Breakout</td>
      <td>74.92</td>
      <td>1.25</td>
    </tr>
    <tr>
      <td>Gryffin - Forever (feat. Elley Duhé) - Lizzy Jane Remix</td>
      <td>72.17</td>
      <td>1.2</td>
    </tr>
    <tr>
      <td>Cash Cash - Bleach (Move On) - VIP Remix</td>
      <td>70.82</td>
      <td>1.18</td>
    </tr>
    <tr>
      <td>REAPER - BARRICADE - Justin Hawkes Remix</td>
      <td>69.88</td>
      <td>1.16</td>
    </tr>
    <tr>
      <td>Adventure Club - Everything To Me - Adventure Club Remix</td>
      <td>69.28</td>
      <td>1.15</td>
    </tr>
    <tr>
      <td>KOLIDESCOPES - Focus</td>
      <td>68.2</td>
      <td>1.14</td>
    </tr>
    <tr>
      <td>Man Cub - Transformation</td>
      <td>68.02</td>
      <td>1.13</td>
    </tr>
    <tr>
      <td>Sun Lo - Nothing Permanent</td>
      <td>66.92</td>
      <td>1.12</td>
    </tr>
    <tr>
      <td>Gryffin - Dreams</td>
      <td>66.42</td>
      <td>1.11</td>
    </tr>
    <tr>
      <td>Ambyion - Floating</td>
      <td>65.38</td>
      <td>1.09</td>
    </tr>
    <tr>
      <td>Lexurus - Cold Divide</td>
      <td>64.95</td>
      <td>1.08</td>
    </tr>
    <tr>
      <td>Puppet - The Answer Is Nowhere</td>
      <td>64.68</td>
      <td>1.08</td>
    </tr>
    <tr>
      <td>Metrik - Parallel - VIP</td>
      <td>64.52</td>
      <td>1.08</td>
    </tr>
    <tr>
      <td>Tommy Baynen - A View From Afar</td>
      <td>63.67</td>
      <td>1.06</td>
    </tr>
    <tr>
      <td>Audioscribe - Letting Go</td>
      <td>63.6</td>
      <td>1.06</td>
    </tr>
    <tr>
      <td>The Chainsmokers - Riptide - lofi remix</td>
      <td>62.53</td>
      <td>1.04</td>
    </tr>
    <tr>
      <td>Eden Project - The Fire</td>
      <td>62.05</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>NOTION - Hooked</td>
      <td>61.8</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>Exploid - Don't Need Nothing</td>
      <td>61.65</td>
      <td>1.03</td>
    </tr>
    <tr>
      <td>OCULA - We Don't Need A Reason Why</td>
      <td>61.43</td>
      <td>1.02</td>
    </tr>
    <tr>
      <td>T & Sugah - Lost In The Middle</td>
      <td>60.68</td>
      <td>1.01</td>
    </tr>
    <tr>
      <td>Tycho - Skate</td>
      <td>60.67</td>
      <td>1.01</td>
    </tr>
    <tr>
      <td>Dubblem - Recollect</td>
      <td>60.23</td>
      <td>1.0</td>
    </tr>
    <tr>
      <td>LO'99 - Stay High - Ray Foxx Remix</td>
      <td>60.03</td>
      <td>1.0</td>
    </tr>
    <tr>
      <td>Cash Cash - Bleach (Move On)</td>
      <td>59.9</td>
      <td>1.0</td>
    </tr>
    <tr>
      <td>Monrroe - Distant Future</td>
      <td>59.13</td>
      <td>0.99</td>
    </tr>
    <tr>
      <td>No Mana - Over & Over</td>
      <td>59.05</td>
      <td>0.98</td>
    </tr>
    <tr>
      <td>Freaks & Geeks - Freefalling</td>
      <td>58.78</td>
      <td>0.98</td>
    </tr>
    <tr>
      <td>Krakota - Dreamweaver</td>
      <td>58.43</td>
      <td>0.97</td>
    </tr>
    <tr>
      <td>Lexurus - Reminiscence</td>
      <td>57.8</td>
      <td>0.96</td>
    </tr>
    <tr>
      <td>Morgan Page - Our Song</td>
      <td>57.7</td>
      <td>0.96</td>
    </tr>
    <tr>
      <td>Digitalz - Beg for It</td>
      <td>57.57</td>
      <td>0.96</td>
    </tr>
    <tr>
      <td>Maduk - Voyager</td>
      <td>57.08</td>
      <td>0.95</td>
    </tr>
    <tr>
      <td>The Chainsmokers - Up & Down</td>
      <td>56.83</td>
      <td>0.95</td>
    </tr>
    <tr>
      <td>Luude - Down Under (feat. Colin Hay) - Majestic Remix</td>
      <td>56.63</td>
      <td>0.94</td>
    </tr>
    <tr>
      <td>Krakota - Take Me There</td>
      <td>56.47</td>
      <td>0.94</td>
    </tr>
    <tr>
      <td>Ghost in Real Life - Youth</td>
      <td>56.35</td>
      <td>0.94</td>
    </tr>
    <tr>
      <td>Phaeleh - Should Be True</td>
      <td>55.87</td>
      <td>0.93</td>
    </tr>
    <tr>
      <td>Sigma - Adrenaline Rush - Subsonic Remix</td>
      <td>54.98</td>
      <td>0.92</td>
    </tr>
    <tr>
      <td>dvine - Allegiance</td>
      <td>54.9</td>
      <td>0.91</td>
    </tr>
    <tr>
      <td>GAR - Komorebi - Original Mix</td>
      <td>54.88</td>
      <td>0.91</td>
    </tr>
    <tr>
      <td>Kele - Tenderoni</td>
      <td>54.65</td>
      <td>0.91</td>
    </tr>
    <tr>
      <td>Grafix - Say It Now</td>
      <td>54.18</td>
      <td>0.9</td>
    </tr>
    <tr>
      <td>Elley Duhé - MONEY ON THE DASH</td>
      <td>53.93</td>
      <td>0.9</td>
    </tr>
    <tr>
      <td>GOLDHOUSE - Reaction</td>
      <td>53.65</td>
      <td>0.89</td>
    </tr>
    <tr>
      <td>Phaeleh - A New Day</td>
      <td>53.45</td>
      <td>0.89</td>
    </tr>
    <tr>
      <td>Tollef - Sunshine</td>
      <td>53.17</td>
      <td>0.89</td>
    </tr>
    <tr>
      <td>Synergy - Made of Glass</td>
      <td>52.53</td>
      <td>0.88</td>
    </tr>
    <tr>
      <td>Ripple - Feels Like</td>
      <td>51.67</td>
      <td>0.86</td>
    </tr>
    <tr>
      <td>MUZZ - Start Again - Lexurus & Dualistic Remix</td>
      <td>50.92</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>deadmau5 - Bridged By A Lightwave</td>
      <td>50.7</td>
      <td>0.85</td>
    </tr>
    <tr>
      <td>Tantrum Desire - Reach VIP</td>
      <td>50.52</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>BLVCK CROWZ - LIFT YOU UP</td>
      <td>50.48</td>
      <td>0.84</td>
    </tr>
    <tr>
      <td>Phetsta - The Sun - 2009 Remix</td>
      <td>50.05</td>
      <td>0.83</td>
    </tr>
    <tr>
      <td>ILLENIUM - Back to You</td>
      <td>50.02</td>
      <td>0.83</td>
    </tr>
    <tr>
      <td>Krakota - Be The Reason</td>
      <td>49.98</td>
      <td>0.83</td>
    </tr>
    <tr>
      <td>Yoe Mase - Getaway Car</td>
      <td>49.57</td>
      <td>0.83</td>
    </tr>
    <tr>
      <td>Loote - Your Side Of The Bed</td>
      <td>49.15</td>
      <td>0.82</td>
    </tr>
    <tr>
      <td>Maduk - Don't Be Afraid</td>
      <td>49.12</td>
      <td>0.82</td>
    </tr>
    <tr>
      <td>Lexurus - Oblivion</td>
      <td>48.9</td>
      <td>0.82</td>
    </tr>
    <tr>
      <td>Aktive - Tell Me</td>
      <td>48.07</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Paraleven - Lucid (feat. Nathan Ball) - Icarus Remix</td>
      <td>47.83</td>
      <td>0.8</td>
    </tr>
    <tr>
      <td>Phaeleh - Outside the Lines</td>
      <td>47.28</td>
      <td>0.79</td>
    </tr>
    <tr>
      <td>Phaeleh - Journey</td>
      <td>47.25</td>
      <td>0.79</td>
    </tr>
    <tr>
      <td>Magnificence - Encore</td>
      <td>46.85</td>
      <td>0.78</td>
    </tr>
    <tr>
      <td>Sub Focus - I Found You - Grafix Remix</td>
      <td>46.57</td>
      <td>0.78</td>
    </tr>
    <tr>
      <td>UTAH - October</td>
      <td>46.17</td>
      <td>0.77</td>
    </tr>
    <tr>
      <td>Dreaming Cooper - Third Dimension - Remastered 2022</td>
      <td>46.08</td>
      <td>0.77</td>
    </tr>
    <tr>
      <td>Drop Frame - The Lantern</td>
      <td>45.88</td>
      <td>0.76</td>
    </tr>
    <tr>
      <td>Koven - Lions - VIP</td>
      <td>45.77</td>
      <td>0.76</td>
    </tr>
    <tr>
      <td>Immortal Girlfriend - Seeker</td>
      <td>44.95</td>
      <td>0.75</td>
    </tr>
    <tr>
      <td>Kathy Brown - Burn</td>
      <td>44.83</td>
      <td>0.75</td>
    </tr>
    <tr>
      <td>36 - Beyond the Heliosphere</td>
      <td>44.7</td>
      <td>0.75</td>
    </tr>
  </table>
</details>

<details>
  <summary>Top 20 artists</summary>
  <table>
    <tr>
      <th>Artist</th>
      <th>Minutes Elapsed</th>
      <th>Hours Elapsed</th>
    </tr>
    <tr>
      <td>Phaeleh</td>
      <td>2254.08</td>
      <td>37.57</td>
    </tr>
    <tr>
      <td>Maduk</td>
      <td>645.67</td>
      <td>10.76</td>
    </tr>
    <tr>
      <td>Koven</td>
      <td>583.83</td>
      <td>9.73</td>
    </tr>
    <tr>
      <td>Lexurus</td>
      <td>582.62</td>
      <td>9.71</td>
    </tr>
    <tr>
      <td>The Chainsmokers</td>
      <td>481.48</td>
      <td>8.02</td>
    </tr>
    <tr>
      <td>Uppermost</td>
      <td>469.62</td>
      <td>7.83</td>
    </tr>
    <tr>
      <td>Sub Focus</td>
      <td>463.97</td>
      <td>7.73</td>
    </tr>
    <tr>
      <td>Tycho</td>
      <td>393.07</td>
      <td>6.55</td>
    </tr>
    <tr>
      <td>deadmau5</td>
      <td>362.7</td>
      <td>6.05</td>
    </tr>
    <tr>
      <td>Gryffin</td>
      <td>355.68</td>
      <td>5.93</td>
    </tr>
    <tr>
      <td>ILLENIUM</td>
      <td>351.18</td>
      <td>5.85</td>
    </tr>
    <tr>
      <td>T & Sugah</td>
      <td>345.77</td>
      <td>5.76</td>
    </tr>
    <tr>
      <td>REAPER</td>
      <td>333.9</td>
      <td>5.57</td>
    </tr>
    <tr>
      <td>Sizzle Bird</td>
      <td>330.42</td>
      <td>5.51</td>
    </tr>
    <tr>
      <td>Puppet</td>
      <td>328.27</td>
      <td>5.47</td>
    </tr>
    <tr>
      <td>Metrik</td>
      <td>309.47</td>
      <td>5.16</td>
    </tr>
    <tr>
      <td>Morgan Page</td>
      <td>290.42</td>
      <td>4.84</td>
    </tr>
    <tr>
      <td>Exploid</td>
      <td>286.45</td>
      <td>4.77</td>
    </tr>
    <tr>
      <td>Grafix</td>
      <td>268.55</td>
      <td>4.48</td>
    </tr>
    <tr>
      <td>Camo & Krooked</td>
      <td>261.57</td>
      <td>4.36</td>
    </tr>
  </table>
</details>

Unfortunately no album data at this point, as Spotify doesn't make it available as part of a regular data export.
