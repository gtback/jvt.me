---
title: "Comparing the different Merge Request / Pull Request merge methods in GitLab and GitHub"
description: "How the different merge methods for contributions work between GitLab and GitHub."
tags:
- blogumentation
- gitlab
- github
date: 2024-01-15T10:19:31+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: gitlab-rebase-merge
image: https://media.jvt.me/c02cdf0130.png
---
Although I've [been a big fan of GitLab for years](https://www.jvt.me/posts/2017/03/25/why-you-should-use-gitlab/), unfortunately every company I've worked for has been centred around GitHub.com or GitHub Enterprise Server (self-hosted).

This means that I've gotten rather used to the way that GitHub does merges, opposed to GitLab.

When I recently rolled out [go-semantic-release](https://github.com/go-semantic-release/semantic-release) on [dependency-management-data (DMD)](https://dmd.tanna.dev) to manage releasing the project based on [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/), I wanted to replace the default merge commits with what GitHub calls the "rebase merge".

(Aside: I didn't investigate using go-semantic-release with a merge commit + conventional commits)

When looking at GitLab's options in the Merge Requests settings page, I wasn't sure which of these would do what I would expect of GitHub:

![A settings widget with three options for merging - "Merge commit" (checked), "Merge commit with semi-linear history" and "Fast-forward merge"](https://media.jvt.me/d5681b4c71.png)

So that's what this post is - me explaining the differences based on [GitHub's documentation](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/about-pull-request-merges) and [GitLab's documentation](https://docs.gitlab.com/ee/user/project/merge_requests/methods/index.html).

## Merge commit

GitLab's [Merge commit method](https://docs.gitlab.com/ee/user/project/merge_requests/methods/index.html#merge-commit) aligns with how [GitHub's merge method](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/about-pull-request-merges#merge-your-commits) works, introducing a merge commit between the two branches.

GitLab's Merge commit method also fulfills [GitHub's "squash merge"](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/about-pull-request-merges#squash-and-merge-your-commits) functionality, allowing the author/merger to squash their commits at that point.

However, GitLab provides much more flexibility around squashing, via their settings page:

![A settings widget with four options under the "Squash commits when merging" setting - "Do not allow", "Allow" (checked), "Encourage", and "Require"](https://media.jvt.me/b4e1744fb3.png)

## Merge commit with semi-linear history

This appears to be similar to GitHub's Merge commit method, with the `Require branches to be up to date before merging` setting enabled, but as its own configuration.

Also, unlike GitHub, when a Merge Request is behind the target branch, it can be rebased:

![The Merge Request UI in GitLab showign that the merge is blocked because "the source branch must be rebased onto the target branch", and giving the user an option to "Rebase", or less emphasised, "Rebase without pipeline"](https://media.jvt.me/dd4e5ec071.png)

This gives you the ability to update the branch that the Merge Request is from, straight from the UI, instead of requiring that you check out the code locally and make the same changes. It's a small quality-of-life improvement, but appreciated!

## Fast-forward merge

GitLab's [Merge commit method](https://docs.gitlab.com/ee/user/project/merge_requests/methods/index.html#fast-forward-merge) aligns with how [GitHub's "rebase merge" method](https://docs.github.com/en/pull-requests/collaborating-with-pull-requests/incorporating-changes-from-a-pull-request/about-pull-request-merges#rebase-and-merge-your-commits), taking each commit from the source branch and rebasing it onto the target branch.
