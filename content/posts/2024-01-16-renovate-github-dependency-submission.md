---
title: "Using `renovate-to-sbom` with the GitHub Dependency Submission API"
description: "How to improve the data in GitHub's Dependency Graph by using an SBOM produced by Renovate data."
tags:
- dependency-management-data
- renovate
- sbom
- github
date: 2024-01-16T10:27:45+0000
license_prose: CC-BY-NC-SA-4.0
license_code: Apache-2.0
slug: renovate-github-dependency-submission
image: "https://media.jvt.me/a51e39d259.jpeg"
---
Built into GitHub repositories - if enabled - is the [Dependency Graph](https://docs.github.com/en/code-security/supply-chain-security/understanding-your-software-supply-chain/about-the-dependency-graph) which gives insight into the dependencies that you use, as well as dependencies that use packages that the given repository produces, too.

Like many things, it's only as good as [the supported ecosystems](https://docs.github.com/en/code-security/supply-chain-security/understanding-your-software-supply-chain/about-the-dependency-graph#supported-package-ecosystems) which cover some of the key ecosystems, but still miss out on a number of popular ecosystems, for instance Docker or Elixir.

So if we wanted to add support for ecosystems not supported by GitHub, how would we do that? Fortunately, GitHub have made it possible to provide your own data via the [Dependency submission API](https://docs.github.com/en/code-security/supply-chain-security/understanding-your-software-supply-chain/using-the-dependency-submission-api), and a big thanks to <span class="h-card"><a class="u-url" href="https://github.com/jdeastwood">James Eastwood</a></span> for this idea, [via this GitHub discussion thread](https://github.com/renovatebot/renovate/discussions/24823#discussioncomment-7155552)!

To be able to submit to this API, however, we need to be able to understand all the dependencies our project supports, which is no small feat. When looking to understand the dependencies within a given repository, I will reach for the excellent [Renovate](https://docs.renovatebot.com/), as it has a tonne of community-sourced ecosystems, and using [my wrapper, `renovate-graph`](https://gitlab.com/tanna.dev/renovate-graph), we can output the discovered package data as a JSON blob.

We can then take this JSON export, and use another tool I've built, [`renovate-to-sbom`](https://www.jvt.me/posts/2023/11/03/renovate-to-sbom/) to convert this to an SPDX or CycloneDX Software Bill of Materials (SBOM) which can then be consumed by GitHub's tooling to add these dependencies to the Dependency Graph.

So what does this actually look like? Below you can see the GitHub Actions workflow you'd need to perform the scanning + submission:

```yaml
name: Discover dependencies via `renovate-graph` and upload via the Dependency Submission API

on:
  workflow_dispatch:
  push:
    branches:
      - main

jobs:
  SBOM-upload:
    runs-on: ubuntu-latest
    permissions:
      id-token: write
      contents: write

    steps:
    - uses: actions/checkout@v4

    - name: Set up Go
      uses: actions/setup-go@v3
      with:
        go-version: stable

    - name: Install `renovate-to-sbom`
      run: |
        go install dmd.tanna.dev/cmd/renovate-to-sbom@latest

    - name: Retrieve package data with renovate-graph
      run: |
        # `--platform local` and the `RG_LOCAL_` environment variables are
        # further documented in
        # https://www.jvt.me/posts/2023/10/13/renovate-graph-local/ and improve
        # the speed of dependency scanning
        npx @jamietanna/renovate-graph@latest --platform local
      env:
        # As well as looking for pending updates, this also resolves version
        # constraints such as `~= 0.3` to `0.3.1`
        RG_INCLUDE_UPDATES: true

        # variables for `--platform local`
        RG_LOCAL_PLATFORM: github
        RG_LOCAL_ORGANISATION: dagger
        RG_LOCAL_REPO: dagger

    - name: Generate SBOM
      run: |
        renovate-to-sbom out/github-dagger-dagger.json --out-format spdx2.3+json --only-include-known-purl-types

    - uses: actions/upload-artifact@v3
      with:
        name: out
        path: out

    - name: SBOM upload
      uses: advanced-security/spdx-dependency-submission-action@v0.0.1
      with:
        filePath: out
        filePattern: github-dagger-dagger.json.spdx2.3.json
```

With this, we're doing a few key things:

- Using `renovate-graph` to perform the dependency analysis, taking care to use the [`--platform local` to improve performance](https://www.jvt.me/posts/2023/10/13/renovate-graph-local/)
- Using `renovate-to-sbom` to convert that data export to an SBOM (in this case, SPDX v2.3 format)
  - Note the `--only-include-known-purl-types` flag, which is due to the GitHub Dependency Submission API being a bit more restrictive of pURL types that are accepted as noted [in this issue](https://github.com/package-url/purl-spec/issues/286) and [this discussion](https://github.com/orgs/community/discussions/87501)
- Using the official GitHub Action to upload the SPDX SBOM to GitHub's API

Once complete, GitHub's dependency graph is now a little more fleshed out, and detects:

- [the `composer/composer:2-bin` Docker image](https://github.com/jamietanna/dagger/blob/1647a10934e57d79e51e123f65d7607f36fcc647/sdk/php/docker/dev.Dockerfile#L3) that can now be seen [in the Dependency Graph](https://github.com/jamietanna/dagger/network/dependencies?q=composer)
- [the Elixir dependency, `absinthe_client`](https://github.com/jamietanna/dagger/blob/1647a10934e57d79e51e123f65d7607f36fcc647/sdk/elixir/mix.exs#L29) that can now be seen [in the Dependency Graph](https://github.com/jamietanna/dagger/network/dependencies?q=absinthe_client).

That wasn't too difficult, was it?

As noted above, GitHub's only got support for some package types, so you may receive errors like:

```json
{
  "message": "invalid package url: in manifest \"github/dagger/dagger\" decoding \"pkg:foo/...\": invalid package url type: foo",
  "documentation_url": "https://docs.github.com/rest/dependency-graph/dependency-submission#create-a-snapshot-of-dependencies-for-a-repository"
}
```

If you are, make sure you're running with `--only-include-known-purl-types`, and if not please [raise an issue](https://gitlab.com/tanna.dev/dependency-management-data/-/issues/new), and I can try and resolve it!

Aside: I'd be remiss to not mention the Open Source project I've been working on for the last year, [dependency-management-data](https://dmd.tanna.dev), which allows you to take data like this, and get insight into end-of-life, unmaintained or deprecated versions of software, as well as giving you SQL and GraphQL access to query the data!
