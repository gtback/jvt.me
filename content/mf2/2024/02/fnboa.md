{
  "date" : "2024-02-15T08:00:10.009919509Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3723" ],
    "start" : [ "2024-02-14T00:00:00Z" ],
    "end" : [ "2024-02-15T00:00:00Z" ],
    "published" : [ "2024-02-15T08:00:10.009919509Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/fnboa",
  "client_id" : "https://www-api.jvt.me/fit"
}
