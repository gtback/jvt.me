{
  "date" : "2024-02-11T08:00:10.25594292Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8635" ],
    "start" : [ "2024-02-10T00:00:00Z" ],
    "end" : [ "2024-02-11T00:00:00Z" ],
    "published" : [ "2024-02-11T08:00:10.25594292Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/eihsd",
  "client_id" : "https://www-api.jvt.me/fit"
}
