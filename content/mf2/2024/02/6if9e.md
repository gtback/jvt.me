{
  "date" : "2024-02-03T22:22:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-03T22:22:00+0000" ],
    "category" : [ "fosdem", "state-of-open-con" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Deffo getting FOMO by seeing friendly faces having a great time at <a href=\"/tags/fosdem/\">#FOSDEM</a> but looking forward to seeing some of y'all at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> 👀"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/6if9e",
  "tags" : [ "fosdem", "state-of-open-con" ],
  "client_id" : "https://indiepass.app/"
}
