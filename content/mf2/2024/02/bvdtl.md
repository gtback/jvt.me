{
  "date" : "2024-02-11T11:32:52.812612411Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://changelog.com/podcast/577" ],
    "published" : [ "2024-02-11T11:32:52.812612411Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "This was such a great episode, there's some excellent learnings about building products, shipping awesome stuff, and just some great vibes"
    } ]
  },
  "kind" : "listens",
  "slug" : "2024/02/bvdtl",
  "client_id" : "https://editor.tanna.dev/"
}
