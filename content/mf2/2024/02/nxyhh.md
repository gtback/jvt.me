{
  "date" : "2024-02-06T08:00:10.117988824Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "11423" ],
    "start" : [ "2024-02-05T00:00:00Z" ],
    "end" : [ "2024-02-06T00:00:00Z" ],
    "published" : [ "2024-02-06T08:00:10.117988824Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/nxyhh",
  "client_id" : "https://www-api.jvt.me/fit"
}
