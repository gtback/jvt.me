{
  "date" : "2024-02-13T08:00:10.628487628Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4723" ],
    "start" : [ "2024-02-12T00:00:00Z" ],
    "end" : [ "2024-02-13T00:00:00Z" ],
    "published" : [ "2024-02-13T08:00:10.628487628Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/ytvob",
  "client_id" : "https://www-api.jvt.me/fit"
}
