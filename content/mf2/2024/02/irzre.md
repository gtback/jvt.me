{
  "date" : "2024-02-16T22:17:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-16T22:17:00+0000" ],
    "category" : [ "open-source" ],
    "bookmark-of" : [ "https://jacobian.org/2024/feb/16/paying-maintainers-is-good/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/02/irzre",
  "tags" : [ "open-source" ],
  "client_id" : "https://indiepass.app/"
}
