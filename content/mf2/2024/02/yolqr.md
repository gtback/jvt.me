{
  "date" : "2024-02-09T08:00:11.193907513Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5174" ],
    "start" : [ "2024-02-08T00:00:00Z" ],
    "end" : [ "2024-02-09T00:00:00Z" ],
    "published" : [ "2024-02-09T08:00:11.193907513Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/yolqr",
  "client_id" : "https://www-api.jvt.me/fit"
}
