{
  "date" : "2024-02-16T13:12:32.787516868Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "A landscape State of Open Con 24 banner, with Jamie's profile picture + talk details in black-and-white, on top of a colourful background",
      "photo" : "https://media.jvt.me/655b504f6c.png"
    } ],
    "published" : [ "2024-02-16T13:12:32.787516868Z" ],
    "category" : [ "state-of-open-con", "soocon24", "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Very excited to see that the videos from <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> <a href=\"/tags/soocon24/\">#SOOCon24</a> are up - so if you missed my talk Quantifying Your Reliance on Open Source Software with <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a>, you can find [the recording on YouTube](https://www.youtube.com/watch?v=xHWmuNDxisw).\r\n\r\nIf you're interested, also check out [the slides](https://talks.jvt.me/dmd-sooc/slides/) and the [full talk writeup](https://www.jvt.me/posts/2024/02/06/dmd-talk-sooc/)."
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/02/tafzj",
  "tags" : [ "state-of-open-con", "soocon24", "dependency-management-data" ],
  "client_id" : "https://editor.tanna.dev/"
}
