{
  "date" : "2024-02-06T09:46:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "photo" : [ {
      "alt" : "The front of a medium dark blue t-shirt, with the dependency-management-data logo over the right hand breast",
      "photo" : "https://media.jvt.me/899a34021f.jpeg"
    }, {
      "alt" : "The back of a medium dark blue t-shirt, with the dependency-management-data website, dmd.tanna.dev, printed",
      "photo" : "https://media.jvt.me/5509a995f1.jpeg"
    } ],
    "published" : [ "2024-02-06T09:46:00+0000" ],
    "category" : [ "dependency-management-data", "state-of-open-con", "soocon24" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Why yes, yes I am wearing a custom <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> t-shirt to <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> <a href=\"/tags/soocon24/\">#SOOCon24</a> 🤓 big thanks to @carol.gg for making it 🚀"
    } ]
  },
  "kind" : "photos",
  "slug" : "2024/02/gefv2",
  "tags" : [ "dependency-management-data", "state-of-open-con", "soocon24" ],
  "client_id" : "https://indiepass.app/"
}
