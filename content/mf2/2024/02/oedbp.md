{
  "date" : "2024-02-03T12:35:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-03T12:35:00+0000" ],
    "category" : [ "fosdem", "diversity-and-inclusion" ],
    "like-of" : [ "https://blahaj.social/@maartje/111867437110647952" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/oedbp",
  "tags" : [ "fosdem", "diversity-and-inclusion" ],
  "client_id" : "https://indiepass.app/"
}
