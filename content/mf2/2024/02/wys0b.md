{
  "date" : "2024-02-10T10:34:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-10T10:34:00+0000" ],
    "category" : [ "go" ],
    "like-of" : [ "https://grafana.com/blog/2024/02/09/how-i-write-http-services-in-go-after-13-years/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/wys0b",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
