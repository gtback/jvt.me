{
  "date" : "2024-02-03T08:00:10.014432656Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6890" ],
    "start" : [ "2024-02-02T00:00:00Z" ],
    "end" : [ "2024-02-03T00:00:00Z" ],
    "published" : [ "2024-02-03T08:00:10.014432656Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/pct72",
  "client_id" : "https://www-api.jvt.me/fit"
}
