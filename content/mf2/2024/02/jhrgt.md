{
  "date" : "2024-02-16T10:39:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/github" ],
    "published" : [ "2024-02-16T10:39:00+0000" ],
    "category" : [ "openapi" ],
    "like-of" : [ "https://github.com/scalar/scalar" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/jhrgt",
  "tags" : [ "openapi" ],
  "client_id" : "https://indiepass.app/"
}
