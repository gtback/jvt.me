{
  "date" : "2024-02-05T08:00:10.77028419Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10161" ],
    "start" : [ "2024-02-04T00:00:00Z" ],
    "end" : [ "2024-02-05T00:00:00Z" ],
    "published" : [ "2024-02-05T08:00:10.77028419Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/6fhw2",
  "client_id" : "https://www-api.jvt.me/fit"
}
