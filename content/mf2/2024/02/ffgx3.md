{
  "date" : "2024-02-07T08:00:10.05791737Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8833" ],
    "start" : [ "2024-02-06T00:00:00Z" ],
    "end" : [ "2024-02-07T00:00:00Z" ],
    "published" : [ "2024-02-07T08:00:10.05791737Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/ffgx3",
  "client_id" : "https://www-api.jvt.me/fit"
}
