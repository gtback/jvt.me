{
  "date" : "2024-02-06T13:42:25.429934595Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://chaosfem.tw/@KatS/111884787914097243" ],
    "published" : [ "2024-02-06T13:42:25.429934595Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Thanks very much, interested to see your thoughts especially with your work on [Syscat](https://www.sysc.at)! Since the last time we spoke I've added a fair bit more useful info to the documentation site, too, so hopefully that'll help.\r\n\r\nYeah I'm a big fan of using SQLite for it, and treating it as an ephemeral, point-in-time view, but I've spoken to a user whose company is taking it and shipping a subset of the data to Google Cloud's SQL offering, which is interesting and something I may need to look at as a \"how would this work if it's long-lived\""
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/02/fqouw",
  "client_id" : "https://editor.tanna.dev/"
}
