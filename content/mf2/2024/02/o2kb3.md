{
  "date" : "2024-02-06T09:48:48.741334892Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-06T09:48:48.741334892Z" ],
    "category" : [ "soocon24", "state-of-open-con" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "A few hours until [my talk](https://stateofopencon2024.sched.com/event/1Xl38/) at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> talk, and I'm now happy with my slides + writeup. Looking forward to share it with y'all 🛸"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/o2kb3",
  "tags" : [ "soocon24", "state-of-open-con" ],
  "client_id" : "https://editor.tanna.dev/"
}
