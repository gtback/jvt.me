{
  "date" : "2024-02-10T08:00:10.277293624Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6006" ],
    "start" : [ "2024-02-09T00:00:00Z" ],
    "end" : [ "2024-02-10T00:00:00Z" ],
    "published" : [ "2024-02-10T08:00:10.277293624Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/mq52b",
  "client_id" : "https://www-api.jvt.me/fit"
}
