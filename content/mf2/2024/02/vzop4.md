{
  "date" : "2024-02-10T10:46:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-10T10:46:00+0000" ],
    "category" : [ "go" ],
    "bookmark-of" : [ "https://grafana.com/blog/2024/02/09/how-i-write-http-services-in-go-after-13-years/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "bookmarks",
  "slug" : "2024/02/vzop4",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
