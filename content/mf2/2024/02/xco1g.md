{
  "date" : "2024-02-07T21:29:37.739363322Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-02-07T21:29:37.739363322Z" ],
    "category" : [ "state-of-open-con", "soocon24" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Well, I'm home after a great couple of days at <a href=\"/tags/state-of-open-con/\">#StateOfOpenCon</a> <a href=\"/tags/soocon24/\">#SOOCon24</a>, which has given me lots to think about. There were some great talks, some really interesting hallway track conversations, and nice to meet friends old and new.\r\n\r\nFirst of all a huge thank you to the organisers - there were so many of you behind the scenes doing such great work to make the event a massive success. I've seen the effort that it can take to do a single track conference let alone 8 tracks(!!!) so it's a huge result, and I hope y'all are gonna have some well deserved rest! \r\n\r\nI'd like to say in particular a bit thank you for the work that @andypiper@macaw.social has been doing in the lead up to the conference to support the speakers, being warm, super helpful and supportive, as well as seeing them busy over the conference helping ensure everything was going well\r\n\r\nAnd a big thanks to @AmandaBrock@hachyderm.io for all her excellent work with OpenUK and State of Open Con 🙌\r\n\r\nI'll definitely be making my way back next year 👀 And I'll be (re)watching talks as they pop up!"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/02/xco1g",
  "tags" : [ "state-of-open-con", "soocon24" ],
  "client_id" : "https://editor.tanna.dev/"
}
