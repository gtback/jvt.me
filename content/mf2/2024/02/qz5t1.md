{
  "date" : "2024-02-11T10:46:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-11T10:46:00+0000" ],
    "category" : [ "gitlab", "remote-work" ],
    "like-of" : [ "https://yorickpeterse.com/articles/what-it-was-like-working-for-gitlab/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/qz5t1",
  "tags" : [ "gitlab", "remote-work" ],
  "client_id" : "https://indiepass.app/"
}
