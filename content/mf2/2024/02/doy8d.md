{
  "date" : "2024-02-14T08:00:11.166190058Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "4252" ],
    "start" : [ "2024-02-13T00:00:00Z" ],
    "end" : [ "2024-02-14T00:00:00Z" ],
    "published" : [ "2024-02-14T08:00:11.166190058Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/02/doy8d",
  "client_id" : "https://www-api.jvt.me/fit"
}
