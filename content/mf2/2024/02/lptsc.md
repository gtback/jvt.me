{
  "date" : "2024-02-10T10:30:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-02-10T10:30:00+0000" ],
    "category" : [ "go" ],
    "like-of" : [ "https://pace.dev/blog/2018/05/09/how-I-write-http-services-after-eight-years.html" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/02/lptsc",
  "tags" : [ "go" ],
  "client_id" : "https://indiepass.app/"
}
