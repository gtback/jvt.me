{
  "date" : "2024-01-29T08:00:10.329008428Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8527" ],
    "start" : [ "2024-01-28T00:00:00Z" ],
    "end" : [ "2024-01-29T00:00:00Z" ],
    "published" : [ "2024-01-29T08:00:10.329008428Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/vs8si",
  "client_id" : "https://www-api.jvt.me/fit"
}
