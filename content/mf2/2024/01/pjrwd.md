{
  "date" : "2024-01-16T08:00:09.930008607Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5902" ],
    "start" : [ "2024-01-15T00:00:00Z" ],
    "end" : [ "2024-01-16T00:00:00Z" ],
    "published" : [ "2024-01-16T08:00:09.930008607Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/pjrwd",
  "client_id" : "https://www-api.jvt.me/fit"
}
