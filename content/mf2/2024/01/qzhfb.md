{
  "date" : "2024-01-07T14:01:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-07T14:01:00+0000" ],
    "repost-of" : [ "https://aus.social/@aby/111711749356469258" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "This is the reason I infrequently post photos, because I want to spend the time to write good alt text, so don't just upload photos with sub-standard descriptions\n\nhttps://aus.social/@aby/111711749356469258"
    } ]
  },
  "kind" : "reposts",
  "slug" : "2024/01/qzhfb",
  "client_id" : "https://indiepass.app/"
}
