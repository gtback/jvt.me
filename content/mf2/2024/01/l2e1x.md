{
  "date" : "2024-01-17T08:00:10.121199506Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "3464" ],
    "start" : [ "2024-01-16T00:00:00Z" ],
    "end" : [ "2024-01-17T00:00:00Z" ],
    "published" : [ "2024-01-17T08:00:10.121199506Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/l2e1x",
  "client_id" : "https://www-api.jvt.me/fit"
}
