{
  "date" : "2024-01-27T08:00:10.407321415Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8333" ],
    "start" : [ "2024-01-26T00:00:00Z" ],
    "end" : [ "2024-01-27T00:00:00Z" ],
    "published" : [ "2024-01-27T08:00:10.407321415Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/rbsyd",
  "client_id" : "https://www-api.jvt.me/fit"
}
