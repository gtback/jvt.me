{
  "date" : "2024-01-10T08:00:10.092732857Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "5947" ],
    "start" : [ "2024-01-09T00:00:00Z" ],
    "end" : [ "2024-01-10T00:00:00Z" ],
    "published" : [ "2024-01-10T08:00:10.092732857Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/u1bbv",
  "client_id" : "https://www-api.jvt.me/fit"
}
