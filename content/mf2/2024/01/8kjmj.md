{
  "date" : "2024-01-07T08:00:10.596253546Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7056" ],
    "start" : [ "2024-01-06T00:00:00Z" ],
    "end" : [ "2024-01-07T00:00:00Z" ],
    "published" : [ "2024-01-07T08:00:10.596253546Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/8kjmj",
  "client_id" : "https://www-api.jvt.me/fit"
}
