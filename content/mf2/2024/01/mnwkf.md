{
  "date" : "2024-01-15T21:30:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-15T21:30:00+0000" ],
    "category" : [ "sql", "sqlite", "databases" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "What's everyones' favourite resources for how to best find which column(s) to index in your database?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/mnwkf",
  "tags" : [ "sql", "sqlite", "databases" ],
  "client_id" : "https://indiepass.app/"
}
