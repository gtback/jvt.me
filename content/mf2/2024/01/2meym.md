{
  "date" : "2024-01-25T15:09:20.229809769Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-25T15:09:20.229809769Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Love hitting publish on a release, and then seeing a \"TODO\" in the body of the notes that you'd not seen all the other times you'd read through it 😅\r\n\r\nRelated: There's [a new oapi-codegen release out](https://github.com/deepmap/oapi-codegen/releases/tag/v2.1.0) 🚀 \r\n\r\nSome big new features, bug fixes and other bits of cleanup"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/2meym",
  "client_id" : "https://editor.tanna.dev/"
}
