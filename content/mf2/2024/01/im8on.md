{
  "date" : "2024-01-23T18:29:10.290970802Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-23T18:29:10.290970802Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "+1 on this, it came up during discussions in [a recent Sustain OSS Podcast](https://podcast.sustainoss.org/216) as something that would absolutely be a useful resource for protecting the community and helping out with difficult situations"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/im8on",
  "client_id" : "https://editor.tanna.dev/"
}
