{
  "date" : "2024-01-05T08:01:15.791557305Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8457" ],
    "start" : [ "2024-01-04T00:00:00Z" ],
    "end" : [ "2024-01-05T00:00:00Z" ],
    "published" : [ "2024-01-05T08:01:15.791557305Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/w9fgo",
  "client_id" : "https://www-api.jvt.me/fit"
}
