{
  "date" : "2024-01-10T08:35:29.933673221Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "listen-of" : [ "https://linearb.io/dev-interrupted/podcast/you-are-what-you-build-making-your-code-more-human" ],
    "published" : [ "2024-01-10T08:35:29.933673221Z" ],
    "post-status" : [ "published" ]
  },
  "kind" : "listens",
  "slug" : "2024/01/uafpg",
  "client_id" : "https://editor.tanna.dev/"
}
