{
  "date" : "2024-01-06T08:00:10.138368235Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6244" ],
    "start" : [ "2024-01-05T00:00:00Z" ],
    "end" : [ "2024-01-06T00:00:00Z" ],
    "published" : [ "2024-01-06T08:00:10.138368235Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/laj9h",
  "client_id" : "https://www-api.jvt.me/fit"
}
