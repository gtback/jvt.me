{
  "date" : "2024-01-19T08:00:10.402980906Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "8040" ],
    "start" : [ "2024-01-18T00:00:00Z" ],
    "end" : [ "2024-01-19T00:00:00Z" ],
    "published" : [ "2024-01-19T08:00:10.402980906Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/4ed7g",
  "client_id" : "https://www-api.jvt.me/fit"
}
