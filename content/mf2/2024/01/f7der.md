{
  "date" : "2024-01-22T08:00:10.059449592Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7791" ],
    "start" : [ "2024-01-21T00:00:00Z" ],
    "end" : [ "2024-01-22T00:00:00Z" ],
    "published" : [ "2024-01-22T08:00:10.059449592Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/f7der",
  "client_id" : "https://www-api.jvt.me/fit"
}
