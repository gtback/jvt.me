{
  "date" : "2024-01-23T13:19:35.306250032Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-23T13:19:35.306250032Z" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Today I've been emailed by both Clever Cloud and Heroku around \"your account hasn't been used in some time so we're gonna delete it\" - coincidence, or is today their \"clean up all the old accounts day\"?"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/a8rr3",
  "client_id" : "https://editor.tanna.dev/"
}
