{
  "date" : "2024-01-03T08:42:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-03T08:42:00+0000" ],
    "category" : [ "vim", "accessibility" ],
    "like-of" : [ "https://tweesecake.social/@Nuno/111352376450899395" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/xmkqd",
  "tags" : [ "vim", "accessibility" ],
  "client_id" : "https://indiepass.app/"
}
