{
  "date" : "2024-01-14T08:00:10.242600299Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10498" ],
    "start" : [ "2024-01-13T00:00:00Z" ],
    "end" : [ "2024-01-14T00:00:00Z" ],
    "published" : [ "2024-01-14T08:00:10.242600299Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/d7yql",
  "client_id" : "https://www-api.jvt.me/fit"
}
