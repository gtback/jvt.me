{
  "date" : "2024-01-15T08:00:09.906135187Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "7839" ],
    "start" : [ "2024-01-14T00:00:00Z" ],
    "end" : [ "2024-01-15T00:00:00Z" ],
    "published" : [ "2024-01-15T08:00:09.906135187Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/nlrxk",
  "client_id" : "https://www-api.jvt.me/fit"
}
