{
  "date" : "2024-01-20T22:38:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://social.lol/@sophie/111789682580628386" ],
    "published" : [ "2024-01-20T22:38:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "> I was concerned that if my build runs every 12 hours, it’ll keep sending webmentions for the same posts. Remy assures me that duplicate webmentions aren’t an issue, as the accepting server will just respond with a 200 if I send a webmention that it’s already seen.\n\nAlthough that should be true, [I found that some folks don't handle it as well](https://www.jvt.me/posts/2019/10/30/reader-mail-webmention-spam/) - my site was deploying multiple times an hour so was a bit noisier, but worth knowing that not every Webmention receiver is equal"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/01/dh5kg",
  "client_id" : "https://indiepass.app/"
}
