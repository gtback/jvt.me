{
  "date" : "2024-01-12T16:12:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://strangeobject.space/@james/111743711535269850" ],
    "published" : [ "2024-01-12T16:12:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Been doing IndieWeb-y things for a few years and recommend [this article's](https://theadhocracy.co.uk/wrote/one-year-in-the-indieweb/) explanation around how the naming between \"IndieWeb\" and \"indie web\" isn't the best to everyone looking at it 🙃"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/01/1bilp",
  "client_id" : "https://indiepass.app/"
}
