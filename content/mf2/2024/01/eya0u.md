{
  "date" : "2024-01-17T22:07:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "in-reply-to" : [ "https://adhoc.systems/replies/dd14105c-a681-4872-befd-bfdad2277703" ],
    "published" : [ "2024-01-17T22:07:00+0000" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "All good, and that sounds good! When I have some spare time I need to revisit [my latest client](https://editor.tanna.dev) and add some features to make it easier to use (including doing the work to actually Open Source it)"
    } ]
  },
  "kind" : "replies",
  "slug" : "2024/01/eya0u",
  "client_id" : "https://indiepass.app/"
}
