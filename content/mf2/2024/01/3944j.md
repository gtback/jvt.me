{
  "date" : "2024-01-20T08:00:10.406346261Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9515" ],
    "start" : [ "2024-01-19T00:00:00Z" ],
    "end" : [ "2024-01-20T00:00:00Z" ],
    "published" : [ "2024-01-20T08:00:10.406346261Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/3944j",
  "client_id" : "https://www-api.jvt.me/fit"
}
