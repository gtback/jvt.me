{
  "date" : "2024-01-21T08:00:10.787702715Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "10234" ],
    "start" : [ "2024-01-20T00:00:00Z" ],
    "end" : [ "2024-01-21T00:00:00Z" ],
    "published" : [ "2024-01-21T08:00:10.787702715Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/zy02g",
  "client_id" : "https://www-api.jvt.me/fit"
}
