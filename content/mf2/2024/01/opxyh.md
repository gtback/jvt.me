{
  "date" : "2024-01-08T08:00:09.829244437Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "9544" ],
    "start" : [ "2024-01-07T00:00:00Z" ],
    "end" : [ "2024-01-08T00:00:00Z" ],
    "published" : [ "2024-01-08T08:00:09.829244437Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/opxyh",
  "client_id" : "https://www-api.jvt.me/fit"
}
