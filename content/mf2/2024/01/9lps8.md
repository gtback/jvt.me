{
  "date" : "2024-01-05T22:16:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-05T22:16:00+0000" ],
    "category" : [ "dependency-management-data" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Been a big week for documentation with <a href=\"/tags/dependency-management-data/\">#DependencyManagementData</a> - I've added significant docs to [the database schema](https://dmd.tanna.dev/schema/) and [GraphQL schema](https://dmd.tanna.dev/graphql/) and have started a [\"Understanding the data model\" cookbook](https://dmd.tanna.dev/cookbooks/data-model/)"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/9lps8",
  "tags" : [ "dependency-management-data" ],
  "client_id" : "https://indiepass.app/"
}
