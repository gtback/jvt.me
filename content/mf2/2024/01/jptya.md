{
  "date" : "2024-01-05T10:47:09.677918227Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "syndication" : [ "https://brid.gy/publish/twitter" ],
    "published" : [ "2024-01-05T10:47:09.677918227Z" ],
    "category" : [ "music" ],
    "post-status" : [ "published" ],
    "content" : [ {
      "html" : "",
      "value" : "Absolutely loving the various versions of [Down Under (original)](https://open.spotify.com/track/3ZZq9396zv8pcn5GYVhxUi) that are out at the moment, like [Luude ft. Colin Hay](https://open.spotify.com/track/53O5nAlq04RKmSBqW0SFvG) to [this super chill cover](https://open.spotify.com/track/6ZOuNLAbh3fKSIxrKArjPH) that's just popped up on my Spotify Release Radar"
    } ]
  },
  "kind" : "notes",
  "slug" : "2024/01/jptya",
  "tags" : [ "music" ],
  "client_id" : "https://editor.tanna.dev/"
}
