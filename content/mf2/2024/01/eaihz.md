{
  "date" : "2024-01-22T11:48:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-22T11:48:00+0000" ],
    "category" : [ "openuk" ],
    "like-of" : [ "https://openuk.uk/the-future-of-open-source-is-uncertain/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/eaihz",
  "tags" : [ "openuk" ],
  "client_id" : "https://indiepass.app/"
}
