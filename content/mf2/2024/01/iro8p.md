{
  "date" : "2024-01-19T10:03:00+0000",
  "deleted" : false,
  "draft" : false,
  "h" : "h-entry",
  "properties" : {
    "published" : [ "2024-01-19T10:03:00+0000" ],
    "category" : [ "gds", "databases", "postgres" ],
    "like-of" : [ "https://gds.blog.gov.uk/2024/01/17/how-we-migrated-our-postgresql-database-with-11-seconds-downtime/" ],
    "post-status" : [ "published" ]
  },
  "kind" : "likes",
  "slug" : "2024/01/iro8p",
  "tags" : [ "gds", "databases", "postgres" ],
  "client_id" : "https://indiepass.app/"
}
