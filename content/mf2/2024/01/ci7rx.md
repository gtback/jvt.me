{
  "date" : "2024-01-30T08:00:10.129580714Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "2822" ],
    "start" : [ "2024-01-29T00:00:00Z" ],
    "end" : [ "2024-01-30T00:00:00Z" ],
    "published" : [ "2024-01-30T08:00:10.129580714Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/ci7rx",
  "client_id" : "https://www-api.jvt.me/fit"
}
