{
  "date" : "2024-01-11T08:00:10.038775206Z",
  "deleted" : false,
  "draft" : false,
  "h" : "h-measure",
  "properties" : {
    "unit" : [ "steps" ],
    "num" : [ "6109" ],
    "start" : [ "2024-01-10T00:00:00Z" ],
    "end" : [ "2024-01-11T00:00:00Z" ],
    "published" : [ "2024-01-11T08:00:10.038775206Z" ]
  },
  "kind" : "steps",
  "slug" : "2024/01/t5msi",
  "client_id" : "https://www-api.jvt.me/fit"
}
